package com.epam;

import com.epam.model.Door;
import com.epam.model.Hall;
import com.epam.model.Hero;
import com.epam.view.GameMenu;

import java.util.Arrays;

public class App {
    public static void main(String[] args) {
       new GameMenu().show();
    }
}
