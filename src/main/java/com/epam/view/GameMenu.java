package com.epam.view;

import com.epam.App;
import com.epam.controller.Controller;
import com.epam.controller.Game;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class GameMenu {
    static final Logger logger = LogManager.getLogger(App.class.getName());
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodMenu;
    private static Scanner input = new Scanner(System.in);

    public GameMenu() {
        controller = new Game();
        menu = new LinkedHashMap<>();
        controller.start();
        menu.put("1", "1 ");
        menu.put("2", "2 ");
        menu.put("3", "3 ");
        menu.put("4", "4 ");
        menu.put("5", "5 ");
        menu.put("6", "6 ");
        menu.put("7", "7 ");
        menu.put("8", "8 ");
        menu.put("9", "9 ");
        menu.put("10", "10 ");

        methodMenu = new LinkedHashMap<>();
        methodMenu.put("1", this::pressButton1);
        methodMenu.put("2", this::pressButton2);
        methodMenu.put("3", this::pressButton3);
        methodMenu.put("4", this::pressButton4);
        methodMenu.put("5", this::pressButton5);
        methodMenu.put("6", this::pressButton6);
        methodMenu.put("7", this::pressButton7);
        methodMenu.put("8", this::pressButton8);
        methodMenu.put("9", this::pressButton9);
        methodMenu.put("10", this::pressButton10);
    }

    public void pressButton1() {
        logger.trace(controller.getDoor(0));
        controller.choose(0);
    }

    public void pressButton2() {
        logger.trace(controller.getDoor(1));
        controller.choose(1);
    }

    public void pressButton3() {
        logger.trace(controller.getDoor(2));
        controller.choose(2);
    }

    public void pressButton4() {
        logger.trace(controller.getDoor(3));
        controller.choose(3);
    }

    public void pressButton5() {
        logger.trace(controller.getDoor(4));
        controller.choose(4);
    }

    public void pressButton6() {
        logger.trace(controller.getDoor(5));
        controller.choose(5);
    }

    public void pressButton7() {
        logger.trace(controller.getDoor(6));
        controller.choose(6);
    }

    public void pressButton8() {
        logger.trace(controller.getDoor(7));
        controller.choose(7);
    }

    public void pressButton9() {
        logger.trace(controller.getDoor(8));
        controller.choose(8);
    }

    public void pressButton10() {
        logger.trace(controller.getDoor(9));
        controller.choose(9);
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.trace("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            menu.put(keyMenu, "0 ");

            if (methodMenu.isEmpty()) {
                logger.trace("!!!!!!!!!!!You win!!!!!!!!!!!!!");
                break;
            }
            try {
                methodMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
            methodMenu.remove(keyMenu);
        } while (controller.checkGameStatus());
        printLose();
    }

    private void printLose() {
        if (!controller.checkGameStatus()) {
            logger.trace("... You lose ...");
        }
    }

    private void outputMenu() {
        logger.trace("Danger is in " + controller.isDangerDoor() + " doors now");
        logger.trace(controller.getHeroPower());
        logger.trace("Doors: ");
        for (String str : menu.values()) {
            System.out.print(str);
        }
    }
}
