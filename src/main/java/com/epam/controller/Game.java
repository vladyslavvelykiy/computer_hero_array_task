package com.epam.controller;

import com.epam.model.Hall;

public class Game implements Controller {
    private Hall hall;

    public Game() {
        hall = new Hall();
    }

    @Override
    public void start() {hall.fillHall();}

    @Override
    public void choose(int value) {
        hall.chooseDoor(value);
    }

    @Override
    public boolean checkGameStatus() {return hall.isGamestatus();}


    @Override
    public int isDangerDoor() {return hall.isDangerDoor(9);}

    @Override
    public String getHeroPower() {return hall.getHero().toString();}

    @Override
    public String getDoor(int value) {return hall.getDoor(value).toString();}

}
