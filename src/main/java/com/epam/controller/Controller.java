package com.epam.controller;

import com.epam.model.Hall;

public interface Controller {
    void start();
    void choose(int value);
    boolean checkGameStatus();
    int isDangerDoor();
    String getHeroPower();
    String getDoor(int doorNumber);
}
