package com.epam.model;

import java.util.Random;

public class Artifact extends Door {
    private String name;
    private int value;
    Random random;



    public Artifact() {
        this.name = "Artifact";
        random = new Random();
        fill();
    }

    @Override
    public void fill() {
        value = random.nextInt(70) + 10;//random 10 - 80
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setValue(int value) {
        this.value = value;
    }
    @Override
    public String toString() {
        return "Artifact: "  + value;
    }
}
