package com.epam.model;

public abstract class  Door {
    abstract void fill();
    abstract String  getName();
    abstract int getValue();
    abstract  void setValue(int value);
    abstract void setName(String name);

}
