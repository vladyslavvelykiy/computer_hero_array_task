package com.epam.model;

import java.util.Random;

public class Hero {
    private int power;


    public Hero() {
        power = 25;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = this.power + power;
    }

    @Override
    public String toString() {
        return "Hero power = " + power;
    }
}
