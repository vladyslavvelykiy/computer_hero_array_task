package com.epam.model;

import java.util.Random;

public class Monster extends Door {
    private String name;
    private int value;
    Random random;

    public Monster() {
        this.name = "Monster";
        random = new Random();
        fill();
    }

    @Override
    public void fill() {
        value = random.nextInt(95) + 5;//random 5 - 100
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Monster =  " + value;
    }
}