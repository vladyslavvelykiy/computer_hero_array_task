package com.epam.model;

import java.util.Arrays;
import java.util.Random;

public class Hall {
    Random random;
    private Hero hero;
    private Artifact artifact;
    private Monster monster;
    private Door[] doors;
    private boolean gamestatus = true;

    public Hall() {
        hero = new Hero();
        doors = new Door[10];
        random = new Random();
    }

    public void chooseDoor(int i) {
        int value;
        value = doors[i].getValue();
        if (checkType(doors[i].getName())) {
            hero.setPower(value);
            doors[i].setValue(0);
            doors[i].setName(null);
        } else {
            hero.setPower(-value);
            doors[i].setValue(0);
            doors[i].setName(null);
            if (hero.getPower() < 0) {
                gamestatus = false;
            }
        }
    }

    public void fillHall() {
        int sumArtifact;
        int sumMonster;
        int counter;
        do {
            counter = fillArtifacts();
            fillMonsters(counter);
            sumArtifact = sumValuesOf("Artifact");
            sumMonster = sumValuesOf("Monster");
        } while (sumMonster > sumArtifact);
    }

    public Door[] getDoors() {
        return doors;
    }

    public Hero getHero() {
        return hero;
    }

    public boolean isGamestatus() {
        return gamestatus;
    }

    public Door getDoor(int value) {
        return doors[value];
    }

    public int isDangerDoor(int value) {
        if (value == -1) {
            return 0;
        }
        if (doors[value].getName() == "Monster") {
            if (hero.getPower() < doors[value].getValue()) {
                return 1 + isDangerDoor(value - 1);
            } else {
                return 0 + isDangerDoor(value - 1);
            }
        } else {
            return 0 + isDangerDoor(value - 1);
        }
    }

    private boolean checkType(String str) {
        if (str == "Artifact") {
            return true;
        }
        return false;
    }

    private void fillMonsters(int count) {
        int counter = count;
        int value;
        do {
            value = random.nextInt(10);
            if (!isReserved(value)) {
                doors[value] = new Monster();
                counter++;
            }
        } while (counter < doors.length);
    }

    private int fillArtifacts() {
        clearArray();
        int counter = 0;
        int value;
        int rnd;
        rnd = random.nextInt(3) + 4;
        do {
            value = random.nextInt(10);
            if (!isReserved(value)) {
                doors[value] = new Artifact();
                counter++;
            }
        } while (counter < rnd);//random 4 - 6  times
        return counter;
    }

    private boolean isReserved(int i) {
        if (doors[i] == null) {
            return false;
        }
        return true;
    }

    private void clearArray() {
        for (int i = 0; i < doors.length; i++) {
            doors[i] = null;
        }
    }

    private int sumValuesOf(String str) {
        int sum = 0;
        for (Door e : doors) {
            if (e.getName().equals(str)) {
                sum = sum + e.getValue();
            }
        }
        return sum;
    }
}
